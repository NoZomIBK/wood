import org.jetbrains.kotlin.gradle.plugin.KotlinPluginWrapper
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

val kotlinVersion = plugins.getPlugin(KotlinPluginWrapper::class.java).kotlinPluginVersion
val springVersion = "2.3.4.RELEASE"
val commonVersion = "[3.4,)"

project.group = "de.comhix"
project.version = "1.0." + (System.getenv("CI_PIPELINE_IID") ?: "0-SNAPSHOT")

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("reflect"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:1.3.9")
    implementation("de.comhix.commons:logging:$commonVersion")
    implementation("de.comhix.commons:kotlin:$commonVersion")
    implementation("de.comhix.commons:mongodb:$commonVersion")
    implementation("com.google.guava:guava:29.0-jre")
    implementation("com.google.code.gson:gson:2.8.6")
    implementation("com.sun.xml.bind:jaxb-impl:2.3.3")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml:2.11.3")
    implementation("com.squareup.okhttp3:okhttp:3.10.0")
    implementation("io.micrometer:micrometer-registry-prometheus:1.2.0")
    implementation("io.github.mweirauch:micrometer-jvm-extras:0.1.4")
    implementation("org.springframework.boot:spring-boot-starter-web:$springVersion")
    implementation("org.springframework.boot:spring-boot-starter-websocket:$springVersion")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.11.3")
    testImplementation("org.amshove.kluent:kluent:1.60")
    testImplementation("org.testng:testng:6.9.10")
}

plugins {
    kotlin("jvm") version "1.4.10"
    id("jacoco")
    id("maven-publish")
    id("signing")
    id("org.jetbrains.dokka") version "0.10.0"
    id("de.undercouch.download") version "4.0.4"
}

jacoco.toolVersion = "0.8.5"

sourceSets {
    main {
        java {
            srcDir("${rootDir}/src/generated/kotlin")
        }
    }
}

tasks.compileKotlin {
    kotlinOptions.jvmTarget = "1.8"
}
tasks.compileTestKotlin {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.test {
    useTestNG()
}

val copyToLib by tasks.creating(Copy::class) {
    from(configurations.compileClasspath)
    into("$buildDir/output/lib")
}

tasks.jar {
    dependsOn(copyToLib)
    manifest {
        attributes["Main-Class"] = "${project.group}.${project.name}.MainKt"
        attributes["Class-Path"] = configurations.compileClasspath.get().joinToString(" ") { "lib/${it.name}" }

        attributes["Implementation-Version"] = project.version
    }
    destinationDirectory.set(file("$buildDir/output"))
    archiveFileName.set("application.${archiveExtension.get()}")
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = false
        csv.isEnabled = false
        html.destination = file("${buildDir}/reports/jacocoHtml")
    }
}

tasks.check {
    dependsOn(tasks.jacocoTestReport)
}

val generateSources: Task by tasks.creating {
    outputs.dir("${rootDir}/src/generated/kotlin")

    doFirst {
        generateFile("${project.group}.${project.name}.info",
                     "Version.kt",
                     """object Version{
    const val BUILD_VERSION = "${project.version}"
    const val BUILD_DATE = "${Date()}"
    const val BUILD_TIMESTAMP = ${Date().time}
}
""")
    }
}

fun generateFile(packageName: String, fileName: String, fileContent: String) {
    val outputDir: File = file("${rootDir}/src/generated/kotlin/${packageName.replace(".", "/")}")
    if (!outputDir.exists()) outputDir.mkdirs()

    Files.write(Paths.get(outputDir.absolutePath, fileName), """package $packageName

$fileContent
""".toByteArray())
}

tasks.compileKotlin {
    dependsOn(generateSources)
}

tasks.dokka {
    outputFormat = "html"
    outputDirectory = "$buildDir/javadoc"
}

val dokkaJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokka)
    destinationDirectory.set(file("$buildDir/output"))
}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
    destinationDirectory.set(file("$buildDir/output"))
}

publishing {
    publications {
        create<MavenPublication>("default") {
            from(components["java"])
            artifact(dokkaJar)
        }
    }
    repositories {
        maven {
            url = uri("$buildDir/repository")
        }
    }
}

signing {
    isRequired = !project.version.toString().endsWith("SNAPSHOT")
}

val runJar by tasks.registering(Exec::class) {
    dependsOn(tasks.jar)
    group = "execution"
    commandLine("java", "-jar", tasks.jar.get().archiveFile.get().asFile.canonicalPath)
}

val versionBadge by tasks.creating(de.undercouch.gradle.tasks.download.Download::class) {
    val version = "${project.version}".replace("-", ".")
    src("https://img.shields.io/badge/Version-$version-blue")
    dest("$buildDir/version.svg")
}