package de.comhix.wood

import de.comhix.commons.logging.info
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be true`
import org.amshove.kluent.`should contain`
import org.testng.annotations.Test

/**
 * @author Benjamin Beeker
 */
class DataNodeTest {
    @Test
    fun `nextNodes empty needed`() {
        info { "nextNodes empty needed" }

        // given
        val node = DataNode(Woods(listOf(Wood(1000,10))), listOf(), BoardConfiguration(listOf()))

        // when
        val next = node.nextNodes()

        // then
        next.isEmpty().`should be true`()
    }

    @Test
    fun `nextNodes multiple woods`() {
        info { "nextNodes multiple woods" }

        // given
        val node = DataNode(Woods(listOf(Wood(1000, 10), Wood(2000, 20))), listOf(Board(100), Board(100)), BoardConfiguration(listOf()))

        // when
        val next = node.nextNodes()

        // then
        next.size `should be equal to` 2
        next[0].neededBoards `should be equal to` listOf(Board(100))
        next[0].price `should be equal to` 10
        next[0].configuration.cuts `should be equal to` listOf(CutBoard(Wood(1000, 10), listOf(Board(100))))

        next[1].neededBoards `should be equal to` listOf(Board(100))
        next[1].price `should be equal to` 20
        next[1].configuration.cuts `should be equal to` listOf(CutBoard(Wood(2000, 20), listOf(Board(100))))
    }

    @Test
    fun `nextNodes multiple cuts in configuration`() {
        info { "nextNodes multiple cuts in configuration" }

        // given
        val node = DataNode(Woods(listOf(Wood(1000, 10))), listOf(Board(100), Board(100)),
                            BoardConfiguration(listOf(CutBoard(Wood(1000, 10), listOf(Board(100))), CutBoard(Wood(2000, 20), listOf(Board(100))))))

        // when
        val next = node.nextNodes()

        // then
        next.size `should be equal to` 2
        next[0].neededBoards `should be equal to` listOf(Board(100))
        next[0].price `should be equal to` 30
        next[0].configuration.cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100), Board(100)))
        next[0].configuration.cuts `should contain` CutBoard(Wood(2000, 20), listOf(Board(100)))

        next[1].neededBoards `should be equal to` listOf(Board(100))
        next[1].price `should be equal to` 30
        next[1].configuration.cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100)))
        next[1].configuration.cuts `should contain` CutBoard(Wood(2000, 20), listOf(Board(100), Board(100)))
    }

    @Test
    fun `nextNodes multiple woods and cuts in configuration`() {
        info { "nextNodes multiple woods and cuts in configuration" }

        // given
        val node = DataNode(Woods(listOf(Wood(1000, 15), Wood(2000, 25))), listOf(Board(100), Board(100)),
                            BoardConfiguration(listOf(CutBoard(Wood(1000, 10), listOf(Board(100))), CutBoard(Wood(2000, 20), listOf(Board(100))))))

        // when
        val next = node.nextNodes()

        // then
        next.size `should be equal to` 4
        next[0].neededBoards `should be equal to` listOf(Board(100))
        next[0].price `should be equal to` 45
        next[0].configuration.cuts `should contain` CutBoard(Wood(1000, 15), listOf(Board(100)))
        next[0].configuration.cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100)))
        next[0].configuration.cuts `should contain` CutBoard(Wood(2000, 20), listOf(Board(100)))

        next[1].neededBoards `should be equal to` listOf(Board(100))
        next[1].price `should be equal to` 55
        next[1].configuration.cuts `should contain` CutBoard(Wood(2000, 25), listOf(Board(100)))
        next[1].configuration.cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100)))
        next[1].configuration.cuts `should contain` CutBoard(Wood(2000, 20), listOf(Board(100)))

        next[2].neededBoards `should be equal to` listOf(Board(100))
        next[2].price `should be equal to` 30
        next[2].configuration.cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100), Board(100)))
        next[2].configuration.cuts `should contain` CutBoard(Wood(2000, 20), listOf(Board(100)))

        next[3].neededBoards `should be equal to` listOf(Board(100))
        next[3].price `should be equal to` 30
        next[3].configuration.cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100)))
        next[3].configuration.cuts `should contain` CutBoard(Wood(2000, 20), listOf(Board(100), Board(100)))
    }

    @Test
    fun `equal nodes in set`() {
        info { "equal nodes in set" }

        // given
        val node = DataNode(Woods(listOf(Wood(1000, 15), Wood(2000, 25))), listOf(Board(100), Board(100)),
                            BoardConfiguration(listOf(CutBoard(Wood(1000, 10), listOf(Board(100))), CutBoard(Wood(2000, 20), listOf(Board(100))))))
        val node2 = DataNode(Woods(listOf(Wood(1000, 15), Wood(2000, 25))), listOf(Board(100), Board(100)),
                             BoardConfiguration(listOf(CutBoard(Wood(1000, 10), listOf(Board(100))), CutBoard(Wood(2000, 20), listOf(Board(100))))))

        val set = setOf(node)

        // when
        val contains = set.contains(node2)

        // then
        contains.`should be true`()
    }

    @Test
    fun `expectedBest price`() {
        info { "expectedBest price" }

        // given
        val node = DataNode(Woods(listOf(Wood(1000, 10))),
                            listOf(Board(100)),
                            BoardConfiguration(listOf(CutBoard(Wood(1000, 10), listOf(Board(990))))))

        // when
        val expectedBest = node.expectedBest

        // then
        expectedBest `should be equal to` 11
    }

    @Test
    fun `expectedBest no remaining`() {
        info { "expectedBest no remaining" }

        // given
        val node = DataNode(Woods(listOf(Wood(1000, 10))),
                            listOf(),
                            BoardConfiguration(listOf(CutBoard(Wood(1000, 10), listOf(Board(990))),CutBoard(Wood(1000, 10), listOf(Board(990))))))

        // when
        val expectedBest = node.expectedBest

        // then
        expectedBest `should be equal to` 20
    }
}