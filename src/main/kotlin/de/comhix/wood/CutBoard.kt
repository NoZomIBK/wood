package de.comhix.wood

/**
 * @author Benjamin Beeker
 */
data class CutBoard(val wood: Wood, val cuts: List<Board>) {
    companion object {
        private const val EXTRA = 10
    }

    val rest: Int by lazy { wood.length - (cuts.map(Board::length).sum() + ((cuts.size - 1) * EXTRA)) }

    fun fit(board: Board): CutBoard? {
        return if (rest < board.length + EXTRA) {
            null
        }
        else {
            val newCuts: MutableList<Board> = mutableListOf()
            newCuts.addAll(cuts)
            newCuts.add(board)
            CutBoard(wood, newCuts)
        }
    }

    override fun toString(): String {
        val cutCollection = cuts.groupBy { it.length }.map { "\t${it.value.size} * ${it.key}" }.joinToString()
        return "${wood.length} $cutCollection \t(rest: $rest)"
    }
}