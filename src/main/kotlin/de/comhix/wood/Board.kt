package de.comhix.wood

import java.util.stream.IntStream

/**
 * @author Benjamin Beeker
 */
data class Board(val length: Int)

class BoardBuilder {
    private val boards: MutableList<Board> = mutableListOf()

    fun with(count: Int, length: Int): BoardBuilder {
        IntStream.range(0, count).forEach { boards.add(Board(length)) }
        return this
    }

    fun build(): List<Board> {
        return boards.sortedBy { -it.length }
    }
}