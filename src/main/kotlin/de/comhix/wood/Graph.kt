package de.comhix.wood

import com.google.common.base.Stopwatch
import de.comhix.commons.logging.info
import java.util.concurrent.TimeUnit.*

/**
 * @author Benjamin Beeker
 */
class Graph(private val woods: List<Wood>, private val boards: List<Board>) {
    fun findMinimum(): BoardConfiguration {
        val startNode = DataNode(Woods(woods), boards, BoardConfiguration(listOf()))
        val nodes = mutableListOf(startNode)

        var bestNode: Node = MaxPriceNode(woods, boards)
        bestNode = HardPriceNode(6684, boards)

        val watch = Stopwatch.createStarted()
        var generated = 1L
        var filtered = 0L
        do {
            val workingNode = nodes.removeLast()
            //            alreadyVisited.add(workingNode)

            //            if (alreadyVisited.size % 100000 == 0)
            val firstNode = if (nodes.isEmpty()) workingNode else nodes[0]
            //            if (randomLimit > 1 && firstNode.neededBoards.size < workingNode.neededBoards.size) {
            //                randomLimit--
            //            }
            if (watch.elapsed(MILLISECONDS) % 10000 == 0L) {
                val lastNode = if (nodes.isEmpty()) workingNode else nodes[nodes.size - 1]

                info { "best price: ${bestNode.price}, current price: ${workingNode.price} nodes: ${nodes.size}" }
                if (watch.elapsed(SECONDS) != 0L)
                    info {
                        "generated: $generated (${generated / watch.elapsed(SECONDS)}/s) filtered: $filtered (${
                            filtered / watch.elapsed(SECONDS)
                        }/s) running: ${watch.elapsed(HOURS)}:${watch.elapsed(MINUTES) % 60}:${watch.elapsed(SECONDS) % 60}"
                    }
                info { "first node:\t${firstNode.price} ${firstNode.expectedBest} ${firstNode.neededBoards.size} ${firstNode.neededBoards}" }
                info { "last node:\t${lastNode.price} ${lastNode.expectedBest} ${lastNode.neededBoards.size} ${lastNode.neededBoards}" }
                info { "${nodes.map { it.neededBoards.size }}" }

                if (watch.elapsed(SECONDS) % 60 == 0L && bestNode is DataNode) {
                    info { "temporary best with price ${bestNode.price}: ${bestNode.configuration}" }
                }
            }

            //            val newNodes: MutableList<DataNode> = mutableListOf()

            if (workingNode.complete && bestNode.price > workingNode.price) {
                info { "found temporary best with price ${workingNode.price}: ${workingNode.configuration}" }
                bestNode = workingNode

                val newNodes = nodes.filter { it.expectedBest < bestNode.price }
                filtered += newNodes.size - nodes.size

                nodes.clear()
                nodes.addAll(newNodes)
            }

            val nextNodes = workingNode.nextNodes()
            generated += nextNodes.size
            //            newNodes.addAll(nodes)
            //            newNodes.addAll(nextNodes)
            //
            //            nodes.clear()

            val nextNodesFiltered = nextNodes.filter { it.expectedBest < bestNode.price }
                    //                    .filter { !alreadyVisited.contains(it) }
                    //                                 .sortedWith { o1, o2 ->
                    //                                     sortBoards(o1, o2)
                    //                                 }
                    .distinctBy {
                        it.configuration.cuts
                    }
            filtered += nextNodes.size - nextNodesFiltered.size

            nodes.addAll(nextNodesFiltered)
        } while (nodes.isNotEmpty())

        return bestNode.configuration
    }

    private fun sortBoards(o1: DataNode, o2: DataNode): Int {
        return if (o1.neededBoards.size == o2.neededBoards.size) {
            o1.expectedBest - o2.expectedBest
        }
        else {
            o1.neededBoards.size - o2.neededBoards.size
        }
    }

    private fun sortPrice(o1: DataNode, o2: DataNode): Int {
        return if (o1.price == o2.price) {
            o1.neededBoards.size - o2.neededBoards.size
        }
        else {
            o1.price - o2.price
        }
    }

    private fun sortPricePerBoard(o1: DataNode, o2: DataNode): Int {
        return o1.pricePerBoard - o2.pricePerBoard
    }

    private fun noSort(o1: DataNode, o2: DataNode): Int {
        return 0
    }
}