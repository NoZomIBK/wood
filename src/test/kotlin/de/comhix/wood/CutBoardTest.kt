package de.comhix.wood

import de.comhix.commons.logging.info
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be null`
import org.amshove.kluent.`should not be null`
import org.testng.annotations.Test

/**
 * @author Benjamin Beeker
 */
internal class CutBoardTest {
    @Test
    fun `fit fits`() {
        info { "fit fits" }

        // given
        val board = CutBoard(Wood(100, 0), listOf(Board(50)))

        // when
        val fitted = board.fit(Board(30))

        // then
        fitted.`should not be null`()
        fitted.rest `should be equal to` 10
    }

    @Test
    fun `fit fits tight`() {
        info { "fit fits tight" }

        // given
        val board = CutBoard(Wood(100, 0), listOf(Board(50)))

        // when
        val fitted = board.fit(Board(40))

        // then
        fitted.`should not be null`()
        fitted.rest `should be equal to` 0
    }

    @Test
    fun `fit tightly too small`() {
        info { "fit tightly too small" }

        // given
        val board = CutBoard(Wood(100, 0), listOf(Board(50)))

        // when
        val fitted = board.fit(Board(41))

        // then
        fitted.`should be null`()
    }

    @Test
    fun `fit too small`() {
        info { "fit too small" }

        // given
        val board = CutBoard(Wood(100, 0), listOf(Board(50)))

        // when
        val fitted = board.fit(Board(45))

        // then
        fitted.`should be null`()
    }

    @Test
    fun `fit multiple fits`() {
        info { "fit multiple fits" }

        // given
        val board = CutBoard(Wood(100, 0), listOf(Board(20), Board(20)))

        // when
        val fitted = board.fit(Board(40))

        // then
        fitted.`should not be null`()
        fitted.rest `should be equal to` 0
    }

    @Test
    fun `fit multiple too small`() {
        info { "fit multiple too small" }

        // given
        val board = CutBoard(Wood(100, 0), listOf(Board(20), Board(20)))

        // when
        val fitted = board.fit(Board(41))

        // then
        fitted.`should be null`()
    }
}