package de.comhix.wood

import kotlin.math.ceil
import kotlin.math.max

/**
 * @author Benjamin Beeker
 */

interface Node {
    val price: Int
    val complete: Boolean
    val configuration: BoardConfiguration
    val pricePerBoard: Int
    val expectedBest: Int
}

class MaxPriceNode(woods: List<Wood>, boards: List<Board>) : Node {
    override val price: Int = woods.map(Wood::price).maxOrNull()!! * boards.size + 1
    override val expectedBest: Int = price
    override val complete: Boolean = false
    override val pricePerBoard: Int = price / boards.size
    override val configuration: BoardConfiguration
        get() = throw IllegalStateException("this dummy has no configuration")
}

class HardPriceNode(override val price: Int, boards: List<Board>) : Node {
    override val expectedBest: Int = price
    override val complete: Boolean = false
    override val configuration: BoardConfiguration
        get() = throw IllegalStateException("this dummy has no configuration")
    override val pricePerBoard: Int = price / boards.size
}

data class DataNode(val woods: Woods, val neededBoards: List<Board>, override val configuration: BoardConfiguration) : Node {
    override val price: Int = configuration.price
    override val pricePerBoard: Int = if (configuration.boards.isEmpty()) 1 else configuration.price / configuration.boards.size
    override val complete: Boolean = neededBoards.isEmpty()
    override val expectedBest: Int by lazy {
        price + if (neededBoards.isEmpty()) {
            0
        }
        else {
            ceil(((max(0, neededBoards.map { it.length }.sum() - configuration.rest(neededBoards.map { it.length }.minOrNull()!!)) / 50) + 1) * 50
                 * woods.bestPricePerLength).toInt()
        }
    }

    fun nextNodes(): List<DataNode> {
        if (neededBoards.isEmpty()) {
            return emptyList()
        }
        val nextNodes: MutableList<DataNode> = mutableListOf()
        val remainingBoards: MutableList<Board> = mutableListOf()
        remainingBoards.addAll(neededBoards)
        val nextBoard = remainingBoards.removeFirst()

        configuration.fits(nextBoard)
                .map {
                    DataNode(woods, remainingBoards, it)
                }
                .forEach {
                    if (!it.configuration.boards.containsAll(configuration.boards)) {
                        throw IllegalStateException("not all existing boards: ${it.configuration.boards} vs ${configuration.boards}")
                    }
                    if (!it.configuration.boards.contains(nextBoard)) {
                        throw IllegalStateException("nextBoard not in boards")
                    }
                    nextNodes.add(it)
                }
        woods.woods.filter { it.length >= nextBoard.length }
                .map {
                    val cut = CutBoard(it, listOf(nextBoard))
                    val newConfiguration = configuration.add(cut)
                    DataNode(woods, remainingBoards, newConfiguration)
                }
                .forEach {
                    if (!it.configuration.boards.containsAll(configuration.boards)) {
                        throw IllegalStateException("not all existing boards: ${it.configuration.boards} vs ${configuration.boards}")
                    }
                    if (!it.configuration.boards.contains(nextBoard)) {
                        throw IllegalStateException("nextBoard not in boards")
                    }
                    nextNodes.add(it)
                }

        return nextNodes
    }
}