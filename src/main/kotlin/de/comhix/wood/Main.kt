package de.comhix.wood

import de.comhix.commons.logging.info
import java.util.logging.LogManager

/**
 * @author Benjamin Beeker
 */
object Main

fun main() {
    LogManager.getLogManager().readConfiguration(Main::class.java.getResourceAsStream("/logging.properties"))

    val woods = listOf(Wood(2000, 330), Wood(2500, 413), Wood(3000, 495))
    val boards = BoardBuilder().with(4, 2100)
            .with(3, 3000)
            .with(13, 856)
            .with(9, 507)
            .with(4, 1336)
            .with(3, 370)
            .build()

    Main.info { "boards: $boards" }

    val configuration = Graph(woods, boards).findMinimum()

    Main.info { "RESULT" }
    configuration.cuts.forEach {
        Main.info { "$it" }
    }
    Main.info { "Price: ${configuration.price.toDouble() / 100}" }
}