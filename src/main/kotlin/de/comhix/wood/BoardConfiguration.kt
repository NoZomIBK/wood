package de.comhix.wood

/**
 * @author Benjamin Beeker
 */
data class BoardConfiguration(val cuts: List<CutBoard>) {
    val price: Int by lazy { cuts.map { it.wood.price }.sum() }
    val boards: List<Board> = cuts.flatMap { it.cuts }

    fun rest(minimumLength: Int): Int = cuts.map { it.rest }.filter { it >= minimumLength }.sum()

    fun add(cut: CutBoard): BoardConfiguration {
        val newCuts = mutableListOf(cut)
        newCuts.addAll(cuts)
        return BoardConfiguration(newCuts)
    }

    fun fits(board: Board): List<BoardConfiguration> {
        val newConfigurations: MutableList<BoardConfiguration> = mutableListOf()
        cuts.map {
            it to it.fit(board)
        }
                .filter { it.second != null }
                .map { it as Pair<CutBoard, CutBoard> }
                .forEach {
                    val newCuts: MutableList<CutBoard> = mutableListOf()
                    newCuts.addAll(cuts)
                    newCuts.remove(it.first)
                    newCuts.add(it.second)

                    newConfigurations.add(BoardConfiguration(newCuts))
                }
        return newConfigurations
    }

    override fun toString(): String {
        return "cuts=$cuts ${cuts.groupBy { it.wood }.map { it.key to it.value.size }}"
    }
}