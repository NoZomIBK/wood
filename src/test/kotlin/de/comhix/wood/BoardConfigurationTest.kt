package de.comhix.wood

import de.comhix.commons.logging.info
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be true`
import org.amshove.kluent.`should contain`
import org.testng.annotations.Test

/**
 * @author Benjamin Beeker
 */
class BoardConfigurationTest {
    @Test
    fun `add to empty configuration`() {
        info { "add to empty configuration" }

        // given
        val configuration = BoardConfiguration(listOf())

        // when
        val newConfiguration = configuration.add(CutBoard(Wood(1000, 10), listOf(Board(100))))

        // then
        newConfiguration.price `should be equal to` 10
        newConfiguration.cuts `should be equal to` listOf(CutBoard(Wood(1000, 10), listOf(Board(100))))
    }

    @Test
    fun `add to configuration`() {
        info { "add to configuration" }

        // given
        val configuration = BoardConfiguration(listOf(CutBoard(Wood(1000, 10), listOf(Board(100)))))

        // when
        val newConfiguration = configuration.add(CutBoard(Wood(1000, 10), listOf(Board(100))))

        // then
        newConfiguration.price `should be equal to` 20
        newConfiguration.cuts `should be equal to` listOf(CutBoard(Wood(1000, 10), listOf(Board(100))), CutBoard(Wood(1000, 10), listOf(Board(100))))
    }

    @Test
    fun `fits none fitting`() {
        info { "fits none fitting" }

        // given
        val configuration = BoardConfiguration(listOf(CutBoard(Wood(1000, 10), listOf(Board(100))), CutBoard(Wood(1000, 10), listOf(Board(100)))))

        // when
        val fits = configuration.fits(Board(990))

        // then
        fits.isEmpty().`should be true`()
    }

    @Test
    fun `fits one fitting`() {
        info { "fits one fitting" }

        // given
        val configuration = BoardConfiguration(listOf(CutBoard(Wood(1000, 10), listOf(Board(100))),
                                                      CutBoard(Wood(1000, 15), listOf(Board(100), Board(100)))))

        // when
        val fits = configuration.fits(Board(890))

        // then
        fits.size `should be equal to` 1
        fits[0].price `should be equal to` 25
        fits[0].cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(890), Board(100)))
        fits[0].cuts `should contain` CutBoard(Wood(1000, 15), listOf(Board(100), Board(100)))
    }

    @Test
    fun `fits multiple fitting`() {
        info { "fits multiple fitting" }

        // given
        val configuration = BoardConfiguration(listOf(CutBoard(Wood(1000, 10), listOf(Board(100))),
                                                      CutBoard(Wood(1000, 10), listOf(Board(100))),
                                                      CutBoard(Wood(1000, 15), listOf(Board(100), Board(100)))))

        // when
        val fits = configuration.fits(Board(890))

        // then
        fits.size `should be equal to` 2
        fits[0].price `should be equal to` 35
        fits[0].cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100)))
        fits[0].cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(890), Board(100)))
        fits[0].cuts `should contain` CutBoard(Wood(1000, 15), listOf(Board(100), Board(100)))

        fits[1].price `should be equal to` 35
        fits[1].cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100)))
        fits[1].cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(890), Board(100)))
        fits[1].cuts `should contain` CutBoard(Wood(1000, 15), listOf(Board(100), Board(100)))
    }

    @Test
    fun `fits multiple equal boards`() {
        info { "fits multiple equal boards" }

        // given
        val configuration = BoardConfiguration(listOf(CutBoard(Wood(1000, 10), listOf(Board(100), Board(100))),
                                                      CutBoard(Wood(1000, 10), listOf(Board(100), Board(100)))))

        // when
        val fits = configuration.fits(Board(100))

        // then
        fits.size `should be equal to` 2
        fits[0].price `should be equal to` 20
        fits[0].cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100), Board(100)))
        fits[0].cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100), Board(100), Board(100)))

        fits[1].price `should be equal to` 20
        fits[1].cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100), Board(100)))
        fits[1].cuts `should contain` CutBoard(Wood(1000, 10), listOf(Board(100), Board(100), Board(100)))
    }
}