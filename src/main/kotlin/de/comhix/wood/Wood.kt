package de.comhix.wood

/**
 * @author Benjamin Beeker
 */
data class Wood(val length: Int, val price: Int)

data class Woods(val woods: List<Wood>) {
    val bestPricePerLength: Double = woods.map { it.price.toDouble() / it.length }.minOrNull()!!
}